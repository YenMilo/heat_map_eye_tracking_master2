import UIKit
import SceneKit
import ARKit
import WebKit
import ReplayKit

class ViewController: UIViewController, ARSCNViewDelegate, RPPreviewViewControllerDelegate,WKUIDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    
    //-----button test-----//
    @IBOutlet weak var ButtonTest: UIButton!
    //---------------------//
    
    let phoneWidth = 375 * 3;
    let phoneHeight = 812 * 3;
    
    var m_data : [UInt8] = [UInt8](repeating: 0, count: 375*3 * 812*3)
    
    //===========學弟加的=====================
    let useCorrection = true
    var correctionCounter = -1
    var correctionPositions: Array<simd_float2> = Array()
    var fourCorners : Array<simd_float2> = Array()
    //=======================================
    
    var positions: Array<simd_float2> = Array()
    let numPositions = 10;
    
    var eyeLasers : EyeLasers?
    var eyeRaycastData : RaycastData?
    var virtualPhoneNode: SCNNode = SCNNode()
    
    var virtualScreenNode: SCNNode = {
        
        let screenGeometry = SCNPlane(width: 1, height: 1)
        screenGeometry.firstMaterial?.isDoubleSided = true
        screenGeometry.firstMaterial?.diffuse.contents = UIColor.green
        
        return SCNNode(geometry: screenGeometry)
    }()
    
    var testSphereStart : SCNNode = {
        let node = SCNNode(geometry: SCNSphere(radius: 0.01))
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.purple
        return node
    }()
    
    var testSphereEnd : SCNNode = {
        let node = SCNNode(geometry: SCNSphere(radius: 0.01))
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
        return node
    }()
    
    var heatMapNode:SCNNode = {
        let node = SCNNode(geometry:SCNPlane(width: 2, height: 2))  // -1 to 1
        
        let program = SCNProgram() //著色器
        program.vertexFunctionName = "heatMapVert"
        program.fragmentFunctionName = "heatMapFrag"
        
        node.geometry?.firstMaterial?.program = program;
        node.geometry?.firstMaterial?.blendMode = SCNBlendMode.add;
        
        return node;
    } ()
    
    var target : UIView = UIView()
    
    //for web view
    var myTextField:UITextField!
    var myWebView:WKWebView!
    var myActivityIndicator:UIActivityIndicatorView!
    
    //for video duration
    var duration = 0.0
    var s = 0
    
    //for video recording
    var isRecord = false
    let recorder = RPScreenRecorder.shared()
    
    //MARK:- for csv
    var eyeTrackDataHeader = "Timestamp,Duration,X,Y,Offset,CurrentURL\n" //for eyetrack data
    var currentURL : String = "https://shopee.tw/"

    //for calibration
    var calibrateTime = 0
    var EyeTimer:Timer?
    
    //for web label
    var timeLabel:UILabel!
    var startTime = Date()
    
    var startButton:UIButton!
    var stopButton:UIButton!
    
    /*-------------------------------------------------------------------------------------------------*/
    
    //hide status bar
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        target.backgroundColor = UIColor.red
        target.frame = CGRect.init(x: 0,y:0 ,width:25 ,height:25) //在(0,0)的坐標產生一個25*25的長方形
        target.layer.cornerRadius = 12.5
        sceneView.addSubview(target) //把 sceneview 添加到 UIView中
        
        // Set the view's delegate
        sceneView.delegate = self
        
        //sceneView.session.delegate = self
        sceneView.automaticallyUpdatesLighting = true
        
        // Show statistics such as fps and timing information
        //sceneView.showsStatistics = true
        
        let device = sceneView.device!
        let eyeGeometry = ARSCNFaceGeometry(device: device)!
        eyeLasers = EyeLasers(geometry: eyeGeometry)
        eyeRaycastData = RaycastData(geometry: eyeGeometry)
        sceneView.scene.rootNode.addChildNode(eyeLasers!) //把 eyeLaser 加入節點
        sceneView.scene.rootNode.addChildNode(eyeRaycastData!) //把 eyeRaycastData 加入節點
        
        virtualPhoneNode.geometry?.firstMaterial?.isDoubleSided = true
        virtualPhoneNode.addChildNode(virtualScreenNode)
        
        sceneView.scene.rootNode.addChildNode(heatMapNode)
        
        //sceneView.scene.rootNode.addChildNode(testSphereStart)
        //sceneView.scene.rootNode.addChildNode(testSphereEnd)
        self.sceneView.scene.rootNode.addChildNode(virtualPhoneNode)
        
        //MARK:- For web UI
        /*------------For WebUI-----------*/
        // get screen size
        let fullScreenSize = UIScreen.main.bounds.size
        // default size
        let goWidth = 100.0
        let actionWidth = (Double(fullScreenSize.width) - goWidth) / 5
        let width1 = Double(fullScreenSize.width)
        let width2 = width1 / 4
        let width3 = width1 / 6
        
        startButton = UIButton(frame: CGRect(x:0, y:0, width: width2, height: width3))
        startButton.setTitle("Start", for: .normal)
        startButton.backgroundColor = .red
        startButton.addTarget(self, action: #selector(ViewController.start), for: .touchUpInside)
        //self.view.addSubview(startButton)
        
        stopButton = UIButton(frame: CGRect(x:width2 * 3, y:0, width: width2, height: width3))
        stopButton.setTitle("Stop", for: .normal)
        stopButton.backgroundColor = .blue
        stopButton.addTarget(self, action: #selector(ViewController.stop), for: .touchUpInside)
        //self.view.addSubview(stopButton)
        
        timeLabel = UILabel(frame: CGRect(x:width2, y:0, width: width2 * 2, height: width1 / 4.3))
        timeLabel.backgroundColor = .white
        timeLabel.text = "0:00"
        timeLabel.textAlignment = .center
        //self.view.addSubview(timeLabel)
        
        // 建立一個 UITextField 用來輸入網址
        myTextField = UITextField(frame: CGRect(x: 0, y: 10.0 + CGFloat(actionWidth), width: fullScreenSize.width, height: 40))
        myTextField.text = currentURL
        
        // 建立 WKWebView
        myWebView = WKWebView(frame: CGRect(x: 0, y: 5+CGFloat(actionWidth), width: fullScreenSize.width, height: fullScreenSize.height - 50))
        
        // 設置委任對象
        myWebView.navigationDelegate = self as? WKNavigationDelegate
        
        myWebView.scrollView.delegate = self as? UIScrollViewDelegate
        
        // 加入到畫面中, 這個註解掉就可以看到眼動軌跡的 heatmap
        //self.view.addSubview(myWebView)
        
        myWebView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
        myWebView.allowsBackForwardNavigationGestures = true
        // 先讀取一次網址
        self.go()
        
    }
    
    //以下都是按鈕
    
    @objc func start(){
        startRecording()
        print("StartRecording")
        
        startTime = Date()
        print("StartTime: \(startTime)")
        
        
    }
    
    @objc func stop() {
        EyeTimer?.invalidate()
        print("Stop")
        stopRecording()
        eyeTrackCsv()
    }
    
    @objc func eyeTrackCsv(){
        // create path for file
        let text1:String = "eyetrack"
        let fileName = "\(text1.lowercased()).csv"
        let path0 = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path0[0]
        // path of file
        let path = NSURL(fileURLWithPath: documentDirectoryPath).appendingPathComponent(fileName)
        
        do {
            try self.eyeTrackDataHeader.write(to:path!, atomically:true, encoding: String.Encoding.utf8)
        } catch  {
            print("Failed to create file")
            print("\(error)")
        }
    }
    
    @objc func timeString(time: TimeInterval) -> String{ //Label 的時間
        let minute = Int(time) / 60 % 60
        let second = Int(time) % 60
        return String(format: "%02i:%02i", minute, second)
    }
    
    @objc func go() {
        // 隱藏鍵盤
        self.view.endEditing(true)

        // 前往網址
        let url = URL(string:myTextField.text!)
        let urlRequest = URLRequest(url: url!)
        myWebView.load(urlRequest)

    }
    
    /*------------------------------------------Web UI End------------------------------------*/
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARFaceTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // MARK: - ARSCNViewDelegate
    
    // Override to create and configure nodes for anchors added to the view's session.
//    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
//        let node = SCNNode()
//        return node
//    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor else { return }
        eyeLasers?.transform = node.transform;
        eyeRaycastData?.transform = node.transform;
        eyeLasers?.update(withFaceAnchor: faceAnchor)
        eyeRaycastData?.update(withFaceAnchor: faceAnchor)
    }
    
    // MARK: - 一直更新資料的地方
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
        virtualPhoneNode.transform = (sceneView.pointOfView?.transform)!
        
        let options : [String: Any] = [SCNHitTestOption.backFaceCulling.rawValue: false,
                                       SCNHitTestOption.searchMode.rawValue: 1,
                                       SCNHitTestOption.ignoreChildNodes.rawValue : false,
                                       SCNHitTestOption.ignoreHiddenNodes.rawValue : false]
        
        testSphereStart.worldPosition = self.eyeRaycastData!.leftEye.worldPosition
        testSphereEnd.worldPosition = self.eyeRaycastData!.leftEyeEnd.worldPosition
        
        let hitTestLeftEye = virtualPhoneNode.hitTestWithSegment(
            from: virtualPhoneNode.convertPosition(self.eyeRaycastData!.leftEye.worldPosition, from:nil),
            to:  virtualPhoneNode.convertPosition(self.eyeRaycastData!.leftEyeEnd.worldPosition, from:nil),
            //from: self.eyeRaycastData!.leftEye.worldPosition,
            //to:  self.eyeRaycastData!.leftEyeEnd.worldPosition,
            options: options)
        
        let hitTestRightEye = virtualPhoneNode.hitTestWithSegment(
            from: virtualPhoneNode.convertPosition(self.eyeRaycastData!.rightEye.worldPosition, from:nil),
            to:  virtualPhoneNode.convertPosition(self.eyeRaycastData!.rightEyeEnd.worldPosition, from:nil),
            //from: self.eyeRaycastData!.rightEye.worldPosition,
            //to:  self.eyeRaycastData!.rightEyeEnd.worldPosition,
            options: options)
        
        if (hitTestLeftEye.count > 0 && hitTestRightEye.count > 0) {

            let coords = screenPositionFromHittest(hitTestLeftEye[0], secondResult:hitTestRightEye[0])
            
            correction(x:coords.x, y:coords.y)
            
            if(isRecord){
                if(duration <= 18000){ //5min *60sec = 300sec*60 = 18000
                    self.duration += 1
                    let now = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "HH:mm:ss"
                    let timet = formatter.string(from: now)
                    let timetaken = timeString(time: TimeInterval(duration/60))
                    let webOffset = self.myWebView.scrollView.contentOffset.y
                    
                    DispatchQueue.main.async {
                        self.timeLabel.text = timetaken
                            // debug 用
                        print("Time:\(timet) Duration:\(self.duration) Timetaken:\(timetaken) x:\(coords.x*2.35) y: \(coords.y*2.35)  offset:\(webOffset) URL:\(self.currentURL)")
                    }
                    // 新增data到 csv
                    
                    let newLine2 = "\(timet),\(self.duration),\(coords.x*2.35),\(coords.y*2.35),\(webOffset),\(self.currentURL)\n"
                    self.eyeTrackDataHeader.append(newLine2)
                }else{
                    eyeTrackCsv()
                    stopRecording()
                }
            }
            
            incrementHeatMapAtPosition(x:Int(coords.x * 3), y:Int(coords.y * 3))  // convert from points to pixels here
            
            let nsdata = NSData.init(bytes: &m_data, length: phoneWidth * phoneHeight)
            heatMapNode.geometry?.firstMaterial?.setValue(nsdata, forKey: "heatmapTexture")
            
            if(fourCorners.endIndex==4){
                DispatchQueue.main.async(execute: {() -> Void in
                    self.target.center = CGPoint.init(x: CGFloat(coords.x), y:CGFloat(coords.y))
                })
            }
        }
    }
    
    func correction(x:Float, y:Float){
        let circlePositios = [[Int]]([[10, 10], [375-10, 10], [10, 812-10], [375-10, 812-10]])
        correctionPositions.append(simd_float2(x, y));
        if(fourCorners.endIndex != 4){
            if(!useCorrection){
                fourCorners.append(simd_float2(0, 0));
                fourCorners.append(simd_float2(0, 0));
                fourCorners.append(simd_float2(0, 0));
                fourCorners.append(simd_float2(0, 0));
            }
            if(fourCorners.endIndex != correctionCounter){
                correctionCounter += 1
                DispatchQueue.main.async(execute: {() -> Void in
                    self.target.center = CGPoint.init(x: CGFloat(circlePositios[self.correctionCounter][0]), y:CGFloat(circlePositios[self.correctionCounter][1]))
                })
            }
            if(correctionPositions.endIndex > 100){
                correctionPositions.removeFirst()
                var total = simd_float2(0,0);
                var max = simd_float2(-10000,-10000);
                var min = simd_float2(10000,10000);
            
                for pos in correctionPositions {
                    total.x += pos.x
                    total.y += pos.y
                    if(pos.x > max.x){max.x = pos.x}
                    if(pos.y > max.y){max.y = pos.y}
                    if(pos.x < min.x){min.x = pos.x}
                    if(pos.y < min.y){min.y = pos.y}
                }
                total.x /= Float(correctionPositions.count)
                total.y /= Float(correctionPositions.count)
                
                if(max.x-total.x<50 && total.x-min.x<50 && max.y-total.y<50 && total.y-min.y<50){
                    fourCorners.append(simd_float2(total.x, total.y));
                    correctionPositions.removeAll()
                }
            }
        }
        print(fourCorners)
        print(fourCorners.endIndex)
    }
    
    func screenPositionFromHittest(_ result1: SCNHitTestResult, secondResult result2: SCNHitTestResult) -> simd_float2 {
        let iPhoneXPointSize = simd_float2(375, 812)  // size of iPhoneX in points
        let iPhoneXMeterSize = simd_float2(0.0623908297, 0.135096943231532)
        
        let xLC = ((result1.localCoordinates.x + result2.localCoordinates.x))
        var x = xLC / (iPhoneXMeterSize.x / 2.0) * iPhoneXPointSize.x + 200
        
        let yLC = -((result1.localCoordinates.y + result2.localCoordinates.y) / 2.0);
        var y = yLC / (iPhoneXMeterSize.y / 2.0) * iPhoneXPointSize.y + 312
        
        // The 312 points adjustment above is presumably to adjust for the Extrinsics on the iPhone camera.
        // I didn't calculate them and instead ripped them from :
        // https://github.com/virakri/eye-tracking-ios-prototype/blob/master/Eyes%20Tracking/ViewController.swift
        // Probably better to get real values from measuring the camera position to the center of the screen.

        if(fourCorners.endIndex==4){
            if(useCorrection){
                let xScale = (375-20)/(fourCorners[3].x-fourCorners[0].x)
                let yScale = (812-20)/(fourCorners[3].y-fourCorners[0].y)
                x = 10+(x-fourCorners[0].x) * xScale
                y = 10+(y-fourCorners[0].y) * yScale
            }
            x = Float.maximum(Float.minimum(x, iPhoneXPointSize.x-1), 0)
            y = Float.maximum(Float.minimum(y, iPhoneXPointSize.y-1), 0)
            
            print("Index4")
        }
        
        // Do just a bit of smoothing. Nothing crazy.
        positions.append(simd_float2(x,y));
        if positions.count > numPositions {
            positions.removeFirst()
        }
        
        var total = simd_float2(0,0);
        for pos in positions {
            total.x += pos.x
            total.y += pos.y
        }
        
        total.x /= Float(positions.count)
        total.y /= Float(positions.count)
        
        return total
    }
    
    /** Note. I'm not using this because I couldn't figure out how to set an MTLTexture to an SCNProgram because Scenekit has terrible
     documentation. That said you should DEFINITELY fix this if you ever plan to use something like this in production.
     So I left it in for reference. **/
    func metalTextureFromArray(_ array:[UInt8], width:Int, height:Int) -> MTLTexture {
        let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: MTLPixelFormat.a8Unorm, width: width, height: height, mipmapped: false)
        
        let texture = self.sceneView.device?.makeTexture(descriptor: textureDescriptor)
        let region = MTLRegion(origin: MTLOriginMake(0, 0, 0), size: MTLSizeMake(width, height, 1))
        texture?.replace(region: region, mipmapLevel: 0, withBytes: array, bytesPerRow: width)
        
        return texture!
    }
    
    func incrementHeatMapAtPosition(x: Int, y: Int) {
        let radius:Int = 46; // in pixels
        let maxIncrement:Float = 25;
        
        for curX in x - radius ... x + radius {
            for curY in y - radius ... y + radius {
                let idx = posToIndex(x:curX, y:curY)
                
                if (idx != -1) {
                    let offset = simd_float2(Float(curX - x), Float(curY - y));
                    let len = simd_length(offset)
                    
                    if (len >= Float(radius)) {
                        continue;
                    }
                    
                    let incrementValue = Int((1 - (len / Float(radius))) * maxIncrement);
                    if (255 - m_data[idx] > incrementValue) {
                        m_data[idx] = UInt8(Int(m_data[idx]) + incrementValue)
                    } else {
                        m_data[idx] = 255
                    }
                }
            }
        }
    }
    
    func posToIndex(x:Int, y:Int) -> Int {
        if (x < 0 || x >= phoneWidth ||
            y < 0 || y >= phoneHeight) {
            return -1;
        }
        return x + y * phoneWidth;
    }
    
    // Mark:- Recording Function
    func startRecording(){
        guard recorder.isAvailable else {
            print("Recording is not available at this time.")
            return
        }
        recorder.startRecording{ [unowned self] (error) in
            guard error == nil else{
                print("There was an error starting the recording.")
                print(error ?? 00)
                return
            }
            print("Start Recording")
            self.duration = 0
            self.isRecord = true
        }
    }
    
    func stopRecording(){
        recorder.stopRecording{[unowned self] (preview,error) in
            print("Stop Recording")
            
            guard preview != nil else{
                print("Preview Controller is not available")
                return
            }
            
            if let unwrappedPreview = preview {
                unwrappedPreview.previewControllerDelegate = self
                self.present(unwrappedPreview, animated: true)
            }
            
            self.isRecord = false
            
        }
    }
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
    }
    
    //to detect changes of url
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let key = change?[NSKeyValueChangeKey.newKey] {
            print("observeValue \(key)") // url value
            currentURL = (key as AnyObject).absoluteString!
            print(currentURL)
            
        }
    }
    
    @IBAction func insertViewTest(){
        self.view.addSubview(startButton)
        self.view.addSubview(stopButton)
        self.view.addSubview(timeLabel)
        self.view.addSubview(myWebView)
    }
    
}

////MARK: Custom Toast message
// 這東西可以跳出提示框框，先留著備用
//extension UIViewController{
//    //Add a toast message on Screen
//    func showToast(message : String){
//        let toastLabel = UILabel(frame: CGRect(x:self.view.frame.size.width/2 - 100, y:self.view.frame.size.height - 100, width: 200, height: 35))
//        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1.0)
//        toastLabel.textColor = UIColor.white
//        toastLabel.textAlignment = .center
//        toastLabel.numberOfLines = 0
//        toastLabel.numberOfLines = 0
//        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
//        toastLabel.text = message
//        toastLabel.alpha = 1.0
//        toastLabel.layer.cornerRadius = 10;
//        toastLabel.clipsToBounds  =  true
//        toastLabel.adjustsFontSizeToFitWidth = true
//        self.view.addSubview(toastLabel)
//        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
//            toastLabel.alpha = 0.0
//        }, completion: {(isCompleted) in
//            toastLabel.removeFromSuperview()
//        })
//    }
//}
